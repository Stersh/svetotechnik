package pro.siper.svetotechnik.Adapters;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import pro.siper.svetotechnik.Activitys.RoomViewActivity;
import pro.siper.svetotechnik.Interfaces.RoomClickHandler;
import pro.siper.svetotechnik.Models.Room;
import pro.siper.svetotechnik.databinding.RoomItemBinding;


public class RoomsAdapter extends RealmRecyclerViewAdapter<Room, RoomsAdapter.RoomViewHolder> {

    public RoomsAdapter(Context context, OrderedRealmCollection<Room> data) {
        super(context, data, true);
    }

    @Override
    public RoomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RoomItemBinding binding = RoomItemBinding.inflate(inflater, parent, false);
        return new RoomViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(RoomViewHolder holder, int position) {
        final Room room = getData().get(position);
        holder.binding.setRoom(room);
        holder.binding.setClick(new RoomClickHandler() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, RoomViewActivity.class);
                intent.putExtra("id", room.getId());
                context.startActivity(intent);
            }

            @Override
            public boolean onLongClick(View view) {
                Log.d("Rooms adapter", room.getTitle() + " clicked!");
                return true;
            }
        });
    }

    class RoomViewHolder extends RecyclerView.ViewHolder {
        RoomItemBinding binding;

        public RoomViewHolder(View v) {
            super(v);
            binding = DataBindingUtil.bind(v);
        }
    }
}