package pro.siper.svetotechnik;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.github.clans.fab.FloatingActionButton;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import io.realm.Realm;
import pro.siper.svetotechnik.Activitys.RoomEditorActivity;
import pro.siper.svetotechnik.Adapters.RoomsAdapter;
import pro.siper.svetotechnik.Models.Room;

public class MainActivity extends AppCompatActivity {

    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        realm.init(this);
        realm = Realm.getDefaultInstance();

        RecyclerView rooms = (RecyclerView) findViewById(R.id.rooms);
        rooms.setLayoutManager(new LinearLayoutManager(this));
        rooms.setAdapter(new RoomsAdapter(this, realm.where(Room.class).findAllAsync()));
        rooms.addItemDecoration(new HorizontalDividerItemDecoration.Builder(this)
                .marginResId(R.dimen.divider_left_margin, R.dimen.divider_right_margin)
                .sizeResId(R.dimen.divider_size)
                .build());
        rooms.setHasFixedSize(true);

        FloatingActionButton addRoomFab = (FloatingActionButton) findViewById(R.id.room_add_fab);
        addRoomFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, RoomEditorActivity.class);
                startActivity(intent);
            }
        });
    }
}
