package pro.siper.svetotechnik.Interfaces;

import android.view.View;


public interface RoomClickHandler {
    void onClick(View view);
    boolean onLongClick(View view);
}
