package pro.siper.svetotechnik.Models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;


public class Room extends RealmObject {
    @PrimaryKey
    private long id;

    @Required
    private String title;
    private float height;
    private float length;
    private float width;
    private long dateAdded;
    private long dateChanged;

    private String extra;
    private float exp;
    private float ledHeight;
    private String usingLamps;
    private String recommendedLamps;
    private float spaceBetweenLamps;
    private long requiredLux;
    private int lampsCounter;

    public int getLampsCounter() {
        return lampsCounter;
    }

    public void setLampsCounter(int lampsCounter) {
        this.lampsCounter = lampsCounter;
    }

    public long getRequiredLux() {
        return requiredLux;
    }

    public void setRequiredLux(long requiredLux) {
        this.requiredLux = requiredLux;
    }

    public String getRecommendedLamps() {
        return recommendedLamps;
    }

    public void setRecommendedLamps(String recommendedLamps) {
        this.recommendedLamps = recommendedLamps;
    }

    public float getSpaceBetweenLamps() {
        return spaceBetweenLamps;
    }

    public void setSpaceBetweenLamps(float spaceBetweenLamps) {
        this.spaceBetweenLamps = spaceBetweenLamps;
    }

    public String getUsingLamps() {
        return usingLamps;
    }

    public void setUsingLamps(String usingLamps) {
        this.usingLamps = usingLamps;
    }

    public long getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(long dateAdded) {
        this.dateAdded = dateAdded;
    }

    public long getDateChanged() {
        return dateChanged;
    }

    public void setDateChanged(long dateChanged) {
        this.dateChanged = dateChanged;
    }

    public float getExp() {
        return exp;
    }

    public void setExp(float exp) {
        this.exp = exp;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public float getLedHeight() {
        return ledHeight;
    }

    public void setLedHeight(float ledHeight) {
        this.ledHeight = ledHeight;
    }

    public float getLength() {
        return length;
    }

    public void setLength(float length) {
        this.length = length;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }
}
