package pro.siper.svetotechnik;

import android.text.TextUtils;

import java.text.SimpleDateFormat;


public class Utils {
    public static boolean isValidText(String text) {
        return !TextUtils.isEmpty(text) && text.length() >= 3;
    }

    public static boolean isValidFloat(String text) {
        try {
            float res = Float.parseFloat(text);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean notZeroFloat(String text) {
        try {
            float res = Float.parseFloat(text);
            return res > 0.0;
        } catch (Exception e) {
            return false;
        }
    }

    public static String floatFormatter(float d)
    {
        if(d == (long) d)
            return String.format("%d",(long)d);
        else
            return String.format("%s",d);
    }

    public static String dateFormatter(long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm dd.MM.yyyy");
        return formatter.format(date);
    }
}
