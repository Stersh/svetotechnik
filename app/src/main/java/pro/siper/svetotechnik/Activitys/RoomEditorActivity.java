package pro.siper.svetotechnik.Activitys;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.rengwuxian.materialedittext.MaterialEditText;

import io.realm.Realm;
import io.realm.RealmResults;
import pro.siper.svetotechnik.Models.Room;
import pro.siper.svetotechnik.R;
import pro.siper.svetotechnik.Utils;

public class RoomEditorActivity extends AppCompatActivity {

    MaterialEditText title;
    MaterialEditText extra;
    MaterialEditText height;
    MaterialEditText width;
    MaterialEditText length;
    MaterialEditText exp;
    MaterialEditText ledHeight;

    MaterialEditText usingLamps;
    MaterialEditText recommendedLamps;
    MaterialEditText spaceBetweenLamps;
    MaterialEditText requiredLux;
    MaterialEditText lampsCounter;
    Realm realm;
    long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_editor);

        Realm.init(this);
        realm = Realm.getDefaultInstance();

        title = (MaterialEditText) findViewById(R.id.title);
        extra = (MaterialEditText) findViewById(R.id.extra);
        height = (MaterialEditText) findViewById(R.id.height);
        width = (MaterialEditText) findViewById(R.id.width);
        length = (MaterialEditText) findViewById(R.id.length);
        exp = (MaterialEditText) findViewById(R.id.exp);
        ledHeight = (MaterialEditText) findViewById(R.id.led_height);
        usingLamps = (MaterialEditText) findViewById(R.id.using_lamps);
        recommendedLamps = (MaterialEditText) findViewById(R.id.recommended_lamps);
        spaceBetweenLamps = (MaterialEditText) findViewById(R.id.space_between_lamps);
        requiredLux = (MaterialEditText) findViewById(R.id.required_lux);
        lampsCounter = (MaterialEditText) findViewById(R.id.lamps_counter);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        id = intent.getLongExtra("id", -1);

        if(id > -1) {
            loadFromRealm(id);
            setTitle("Редактирование помещения");
        } else {
            setTitle("Добавление помещения");
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateTextFields()) {
                    realm.beginTransaction();

                    long time = System.currentTimeMillis();

                    Room room;
                    if(id > -1) {
                        room = realm.where(Room.class).equalTo("id", id).findFirst();
                    } else {
                        long newId;
                        try {
                            newId = (long) realm.where(Room.class).max("id") + 1;
                        } catch (NullPointerException e) {
                            newId = 0;
                        }
                        room = realm.createObject(Room.class, newId);

                        room.setDateAdded(time);
                    }

                    room.setDateChanged(time);

                    room.setTitle(title.getText().toString());
                    room.setExtra(extra.getText().toString());

                    room.setHeight(Float.parseFloat(height.getText().toString()));
                    room.setWidth(Float.parseFloat(width.getText().toString()));
                    room.setLength(Float.parseFloat(length.getText().toString()));

                    room.setExp(Float.parseFloat(exp.getText().toString()));
                    room.setLedHeight(Float.parseFloat(ledHeight.getText().toString()));

                    room.setUsingLamps(usingLamps.getText().toString());
                    room.setRecommendedLamps(recommendedLamps.getText().toString());

                    if(!TextUtils.isEmpty(spaceBetweenLamps.getText().toString())) {
                        room.setSpaceBetweenLamps(Float.parseFloat(
                                spaceBetweenLamps.getText().toString()));
                    } else {
                        room.setSpaceBetweenLamps(0);
                    }
                    if(!TextUtils.isEmpty(requiredLux.getText().toString())) {
                        room.setRequiredLux(Long.parseLong(requiredLux.getText().toString()));
                    } else {
                        room.setRequiredLux(0);
                    }
                    if(!TextUtils.isEmpty(lampsCounter.getText().toString())) {
                        room.setLampsCounter(Integer.parseInt(lampsCounter.getText().toString()));
                    } else {
                        room.setLampsCounter(0);
                    }

                    realm.copyToRealmOrUpdate(room);
                    realm.commitTransaction();
                    Toast.makeText(RoomEditorActivity.this, "Помещение добавлено",
                            Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }

    public boolean validateTextFields() {
        if(!Utils.isValidText(title.getText().toString())) {
            title.setError("Поле должно содержать 3 и более символов");
            return false;
        } else {
            title.setError(null);
        }
        if(!Utils.isValidFloat(height.getText().toString()) || !Utils.notZeroFloat(height.getText().toString())) {
            height.setError("Поддерживаются только значения с плавающей точкой");
            return false;
        } else {
            height.setError(null);
        }
        if(!Utils.isValidFloat(width.getText().toString()) || !Utils.notZeroFloat(width.getText().toString())) {
            width.setError("Поддерживаются только значения с плавающей точкой");
            return false;
        } else {
            width.setError(null);
        }
        if(!Utils.isValidFloat(length.getText().toString()) || !Utils.notZeroFloat(length.getText().toString())) {
            length.setError("Поддерживаются только значения с плавающей точкой");
            return false;
        } else {
            length.setError(null);
        }
        if(TextUtils.isEmpty(exp.getText().toString())) {
            exp.setText("1.0");
        }
        if(!Utils.isValidFloat(exp.getText().toString())) {
            exp.setError("Поддерживаются только значения с плавающей точкой");
            return false;
        } else {
            if(Float.parseFloat(exp.getText().toString()) > 1.0) {
                exp.setError("Коэффициент эксплуатации не может быть больше 1");
                return false;
            }
            exp.setError(null);
        }
        if(TextUtils.isEmpty(ledHeight.getText().toString())) {
            ledHeight.setText(height.getText().toString());
        }
        if(!Utils.isValidFloat(ledHeight.getText().toString())) {
            ledHeight.setError("Поддерживаются только значения с плавающей точкой");
            return false;
        } else {
            if(Float.parseFloat(ledHeight.getText().toString())
                    > Float.parseFloat(height.getText().toString())) {
                ledHeight.setError("Высота подвеса не может быть больше чем высота помещения");
                return false;
            }
            ledHeight.setError(null);
        }
        if(!TextUtils.isEmpty(spaceBetweenLamps.getText().toString())) {
            if(!Utils.isValidFloat(spaceBetweenLamps.getText().toString())) {
                spaceBetweenLamps.setError("Поддерживаются только значения с плавающей точкой");
                return false;
            } else {
                spaceBetweenLamps.setError(null);
            }
        }

        return true;
    }

    private void loadFromRealm(long id) {
        Room room = realm.where(Room.class).equalTo("id", id).findFirst();
        title.setText(room.getTitle());
        extra.setText(room.getExtra());
        width.setText(Float.toString(room.getWidth()));
        height.setText(Float.toString(room.getHeight()));
        length.setText(Float.toString(room.getLength()));
        ledHeight.setText(Float.toString(room.getLedHeight()));
        exp.setText(Float.toString(room.getExp()));
        usingLamps.setText(room.getUsingLamps());
        recommendedLamps.setText(room.getRecommendedLamps());
        spaceBetweenLamps.setText(Float.toString(room.getSpaceBetweenLamps()));
        requiredLux.setText(Long.toString(room.getRequiredLux()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
