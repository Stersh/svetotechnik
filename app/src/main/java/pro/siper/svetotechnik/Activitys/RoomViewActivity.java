package pro.siper.svetotechnik.Activitys;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.clans.fab.FloatingActionButton;

import java.text.SimpleDateFormat;

import io.realm.Realm;
import io.realm.RealmResults;
import pro.siper.svetotechnik.Models.Room;
import pro.siper.svetotechnik.R;
import pro.siper.svetotechnik.Utils;
import pro.siper.svetotechnik.databinding.ActivityRoomViewerBinding;


public class RoomViewActivity extends AppCompatActivity {

    Toolbar toolbar;
    CollapsingToolbarLayout collapsingToolbar;
    long id;
    Realm realm;
    Room room;
    ActivityRoomViewerBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Realm.init(this);
        id = getIntent().getLongExtra("id", 0);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_room_viewer);

        toolbar = (Toolbar) findViewById(R.id.anim_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RoomViewActivity.this, RoomEditorActivity.class);
                intent.putExtra("id", id);
                startActivity(intent);
            }
        });

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (verticalOffset == 0)
                {
                    fab.show(true);
                }
                else
                {
                    fab.hide(true);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        realm = Realm.getDefaultInstance();

        room = realm.where(Room.class).equalTo("id", id).findFirst();

        collapsingToolbar.setTitle(room.getTitle());
        binding.setRoom(room);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.delete_room:
                showDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.room_editor_menu, menu);
        return true;
    }

    private void showDialog() {
        new MaterialDialog.Builder(this)
                .positiveText("ОК")
                .negativeText("ОТМЕНА")
                .content("Удалить помещение " + room.getTitle() + "?")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                RealmResults<Room> result
                                        = realm.where(Room.class).equalTo("id", id).findAll();
                                result.deleteAllFromRealm();
                            }
                        });
                        if(dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        finish();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        if(dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    }
                })
                .show();
    }
}
